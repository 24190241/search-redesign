function openMenu() {
    var elm = document.getElementById("horizontal");
    var btn = document.getElementById("menuOpen");
    var cls = document.getElementById("menuClose");
    elm.style.display = "block";
    btn.style.display = "none";
    cls.style.top = "0";
    cls.style.paddingLeft = "5px";
}
function closeMenu() {
    var elm = document.getElementById("horizontal");
    var btn = document.getElementById("menuOpen");
    var cls = document.getElementById("menuClose");
    elm.style.display = "none";
    btn.style.display ="block";
    cls.style.top = "50px";
}
